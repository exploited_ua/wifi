
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= jquery.ddslick.min.js

//initialization slide
var Slider = new Swiper('.slider-block', {
    slidesPerView: '1',
    pagination: '.slider-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    autoplay: 2000
});

var Slider1 = new Swiper('.swiper2', {
    slidesPerView: 'auto',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true
});

var Slider2 = new Swiper('.swiper1', {
    slidesPerView: 'auto',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    paginationClickable: true
});

// initialization language select
$('#myDropdown').ddslick({
    onSelected: function(selectedData){
    }
});

// open feedback modal
function openFeedbackModal() {
    $('#feedback').show();
}

$('#btn_feedback').on('click', function (e) {
    e.preventDefault();
    openFeedbackModal();
});

// close modal
function closeModal(){
    $('#conditions').hide();
    $('#feedback').hide();
    $('#succsess_feedback').hide();
}

$('.close_modal').on('click', function () {
    closeModal();
});

// open Succsess Feedback Modal

function openSuccsessFeedbackModal() {
    $('#feedback').hide();
    $('#succsess_feedback').show();
}

$('.btn_confirm').on('click', function (e) {
    e.preventDefault();
    openSuccsessFeedbackModal();
});

// open Terms and Conditions

function openConditionsModal() {
    $('#conditions').show();
}

$("#registration-form-terms-and-conditions-text").on('click', function () {
    openConditionsModal();
});

// open After Registration block
function openAfterRegistration() {
    $('.after-registration').show().css('display','flex');
}

$("#registration-form-submit-button").on('click', function (e) {
    e.preventDefault();
    openAfterRegistration();
});


window.onclick = function(event) {
    if (event.target == $('#conditions')) {
        $('#conditions').hide();
    }else if(event.target == $('#feedback')){
        $('#feedback').hide();
    }else if(event.target == $('#succsess_feedback')){
        $('#succsess_feedback').hide();
    }
};


